<style>
.skynote-bg { background: #2fa8df !important; margin-top: 50px; padding: 25px 0; }
.single-tour-item .mkdf-container-inner { padding-bottom: 0 !important; }
.skynote-bg .vc_col-sm-3 vc_column-inner { padding: 0; }
</style>
<?php 

$sky_notes = get_posts( 
    array(
    	'name' => 'skydive-notes',
        'post_type' => 'page',
        'posts_per_page' => 1,
    )
);
if ($sky_notes): foreach ($sky_notes as $post): setup_postdata($post);

echo do_shortcode(get_the_content());

endforeach;
endif;
wp_reset_postdata();
?>