<style>
#skydive-options .mkdf-tours-row.mkdf-tours-columns-3 .mkdf-tours-row-item { width: 32.33333%; padding: 5px; }
html.tour-pop-open{ overflow: hidden !important; }
.mkdf-sticky-header.tour-pop-open, .skydive-vidphoto-table .wpb_gallery_slides.flexslider .flex-control-nav, .skydive-vidphoto-table .mkdf-price-table .mkdf-pt-button, .per-video { display:none !important; }
.skydive-batemans-bay .per-video { display: block !important; }
.option-item{
	display:none;
	background: rgba(0,0,0,0.75);
	position: fixed;
	width: 100%;
	z-index: 9999;
	height: 100%;
	top: 0;
	left: 0;
	opacity: 0;
	transition: visibility 0s, opacity 0.5s linear;
	-webkit-transition: visibility 0s, opacity 0.5s linear;
	-moz-transition: visibility 0s, opacity 0.5s linear;
}
.option-wrapper {
	max-width: 500px;
	margin: 6% auto 0;
	border-radius: 15px;
	position: fixed;
	left: 0;
	right: 0;
	background: #fff;
	max-height: 728px;
	overflow-y: auto;
	overflow-x: hidden;
}
.option-wrapper::-webkit-scrollbar {
    width: 10px;
}
.option-wrapper::-webkit-scrollbar-track {
    background: #f1f1f1;
    border-radius: 5px; 
}
.option-wrapper::-webkit-scrollbar-thumb {
    background: #888; 
}
.option-wrapper::-webkit-scrollbar-thumb:hover {
    background: #555; 
}
.pop-title {
    text-align: center;
    color: #4fb1e3;
    font-weight: 600;
    font-size: 16px;
    padding: 10px 0;
}
.pop-close {
position: absolute;
border-radius: 100%;
background: #4fb1e3;
padding: 5px;
font-size: 15px;
width: 16px;
height: 16px;
text-align: center;
line-height: 1;
font-weight: bold;
color: #fff;
right: -2px;
top: -2px;
}
#skydive-options { margin-bottom: 61px;}
#skydive-options .mkdf-tours-revealing-item .mkdf-tours-revealing-item-content-holder { padding: 0; }
#skydive-options .mkdf-tours-revealing-item .mkdf-tours-revealing-title-holder { padding: 15px 25px 10px; }
#skydive-options .mkdf-tours-revealing-item-excerpt { padding: 0px 15px; }
#skydive-options .book-now-btn { margin: 0 -2px; }
#skydive-options .mkdf-tour-title .option-title { font-size: 32px; }
#skydive-options .mkdf-tour-title .option-sub { font-size: 18px;letter-spacing: -1px; }
#skydive-options .mkdf-tours-item-price { color: #7ed16f !important; }
#skydive-options .sale-now { position: absolute;top: 0;right: 0px;}

.mkdf-tour-gallery, .mkdf-tour-gallery > .vc_row { margin: 0 !important; }

.skydive-vidphoto-table .wpb_column .vc_column-inner { padding: 0 5px; }
.skydive-vidphoto-table .wpb_gallery_slides.flexslider { margin: 0; border: 0; }
.skydive-vidphoto-table .mkdf-price-table .mkdf-pt-inner ul li { padding: 10px 15px; }
.skydive-vidphoto-table .mkdf-price-table .mkdf-pt-content ul li { padding: 10px 0; }
.skydive-vidphoto-table .mkdf-price-table .mkdf-pt-title { min-height: 118px; }
.skydive-vidphoto-table .mkdf-price-table .mkdf-pt-mark { min-height: 42px; } 
.skydive-vidphoto-table .mkdf-price-table .mkdf-pt-inner > ul { padding-bottom: 20px; }

@media ( max-width: 1200px ) {
	.skydive-vidphoto-table .wpb_column { width: 50% !important; }
}

@media ( max-width: 768px ){
	#skydive-options .mkdf-tours-row.mkdf-tours-columns-3 .mkdf-tours-row-item { width: auto; }
	.option-wrapper { position: relative; border: 1px solid #e1e1e1; max-width: 100% !important; overflow: auto !important;}
	.option-item { background: transparent; position: relative; }
	html.tour-pop-open{ overflow: auto !important; }
	.mkdf-tour-item-single-holder .mkdf-grid-col-9 { padding: 0 !important; }
	.book-now-btn img { width: 100%; }
}

@media ( max-width: 767px ){
	.skydive-vidphoto-table .wpb_column { width: 100% !important; }
}
</style>
<script type="text/javascript" src="<?php echo plugins_url( 'img/pluginJS.js', __FILE__ ); ?>"></script>
<div id="skydive-options" class="mkdf-tours-search-content">
   <div class="mkdf-tours-row mkdf-tr-normal-space mkdf-tours-columns-3">
      <div class="mkdf-tours-row-inner-holder" style="margin: 0;">
	 <h3 class="mkdf-gallery-title">
            <?php esc_html_e('Skydive Options', 'mkdf-tours'); ?>
        </h3>
	<?php
	global $post;
    	$post_slug = $post->post_name . '-options'; 
	$sky_options = get_posts( 
	    array(
	        'post_type' => 'tour-item',
	        'posts_per_page' => -1,
                'post_status' => 'publish',
                'tax_query' => array(
			array(
				'taxonomy' => 'tour-category',
				'field'    => 'slug',
				'terms'    => $post_slug,
			),
		),
		'meta_key' => 'mkdf_tours_price',
		'orderby' => 'meta_value_num',
		'order' => 'ASC',
	    )
	);
	if ($sky_options): foreach ($sky_options as $post): setup_postdata($post);
	$old_price = get_post_meta(get_the_ID(), 'mkdf_tours_price', true);
	$new_price = get_post_meta(get_the_ID(), 'mkdf_tours_discount_price', true);
	$content = get_the_content();
	?>
		<?php if(has_post_thumbnail()) : ?>
		<div <?php post_class(array('mkdf-tours-revealing-item mkdf-tours-row-item',mkdf_tours_get_tour_rating_class())); ?>>
			<div class="mkdf-tours-revealing-item-image-holder">
				
				<div class="mkdf-tours-revealing-item-image">
					<?php the_post_thumbnail('full'); ?>
					<?php if ($new_price): ?>
					<span class="sale-now">
						<img src="<?php echo plugins_url( 'img/sale-corner-right.png', __FILE__ ); ?>" />
					</span>
					<?php endif; ?>
					<div class="mkdf-tours-revealing-item-content-holder">
						<div class="mkdf-tours-revealing-item-content-inner">
							<div class="mkdf-tours-revealing-title-holder">
								<h4 class="mkdf-tour-title">
									<span class="option-title"><?php the_title(); ?></span>
									<span class="option-sub">Tandem Skydive</span>
									
								</h4>
								<span class="mkdf-tours-revealing-item-price-holder">
								<?php if ($new_price):?>
									<span class="mkdf-tours-price-holder mkdf-tours-price-with-discount">
										<span class="mkdf-tours-item-price mkdf-tours-price-old">$<?php echo $old_price;?></span>
										<span class="mkdf-tours-item-discount-price mkdf-tours-item-price">$<?php echo $new_price;?></span>
									</span>
								<?php else: ?>
								<span class="mkdf-tours-price-holder">
									<span class="mkdf-tours-item-price ">$<?php echo $old_price;?></span>
								</span>
								<?php endif;?>
								</span>
							</div>
							<div class="book-now-btn">
								<img src="<?php echo plugins_url( 'img/button.png', __FILE__ ); ?>" > 
							</div>
							<?php if(has_excerpt()) : ?>
								<div class="mkdf-tours-revealing-item-excerpt">
									<div class="mkdf-tours-revealing-item-excerpt-inner">
										<?php the_excerpt(); ?>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<a id="option-link-<?php the_ID();?>" class="mkdf-tours-revealing-item-link" href="#skydive-options" data-id="<?php the_ID();?>"></a>
				<div id="option-content-<?php the_ID();?>" class="option-item">
				<div class="option-wrapper">
				<a href="#" class="pop-close">X</a>
				<div class="pop-title"><?php the_title(); ?> Tandem Skydive</div>
				<?php echo $content; ?>
				</div>
				</div>
				
			</div>
			</div>
		<?php endif; ?>
		<?php endforeach; wp_reset_postdata();?>
		<?php else: ?>
		   <p>There is no available Skydive Option for this location.</p>
		<?php endif; ?>
	</div>
      </div>
   </div>
<script>
jQuery(document).ready(function(){
function option_pop(id){
  var option_id = '#option-content-' + id; 
  jQuery(option_id).css({
  	'display' : 'block',
  	'opacity' : '1'
  });
    jQuery(document).on('click','.pop-close',function(e){
      jQuery(option_id).hide();
      jQuery('header .mkdf-sticky-header').removeClass('tour-pop-open');
      jQuery('html').removeClass('tour-pop-open');
      e.preventDefault();
  });
}
jQuery(document).on('click','.mkdf-tours-revealing-item-link',function(e){
var data_id = jQuery(this).data('id');
option_pop(data_id);
jQuery('html').addClass('tour-pop-open');
jQuery('header .mkdf-sticky-header').addClass('tour-pop-open');
e.preventDefault();
jQuery(this).parent().find('iframe').attr( 'src', function ( i, val ) { return val; });
});
});
</script>