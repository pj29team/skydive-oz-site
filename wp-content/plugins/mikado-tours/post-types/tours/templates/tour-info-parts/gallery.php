<?php
global $post;
$post_name = $post->post_name;
$gallery_subtitle   = get_post_meta(get_the_ID(), 'mkdf_tours_gallery_subtitle', true);
$gallery_excerpt   = get_post_meta(get_the_ID(), 'mkdf_tours_gallery_excerpt', true);
$image_gallery_val = get_post_meta(get_the_ID(), 'mkdf_tours_gallery_images', true);

if($image_gallery_val !== '') : ?>

    <div class="mkdf-tour-gallery-item-holder <?php echo $post_name; ?>">

        <h3 class="mkdf-gallery-title">
            <?php esc_html_e('Photo & Video Options', 'mkdf-tours'); ?>
        </h3>

        <h5 class="mkdf-tour-gallery-item-subtitle">
            <?php echo wp_kses_post($gallery_subtitle); ?>
        </h5>

        <p class="mkdf-tour-gallery-item-excerpt">
            <?php echo wp_kses_post($gallery_excerpt); ?>
        </p>

        <div class="mkdf-tour-gallery clearfix">
	<?php 
	$vidphoto_options = get_posts( 
		array(
		'post_type' => 'page',
		'posts_per_page' => 1,
		'post_status' => 'publish',
		'pagename' => 'video-photo-options'
		)
	);
	if ($vidphoto_options): foreach ($vidphoto_options as $post): setup_postdata($post); 
		the_content();
	endforeach; 
	wp_reset_postdata(); 
	else: ?>
	<p>There is no available Skydive Video and Photo Options.</p>
	<?php endif; ?>
        </div>
    </div>
<?php endif; ?>