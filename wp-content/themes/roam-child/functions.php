<?php

/*** Child Theme Function  ***/

function skyoz_dequeue_script() {
   wp_dequeue_script( 'roam_mikado_google_map_api' );
}
add_action( 'wp_print_scripts', 'skyoz_dequeue_script', 100 );

function roam_mikado_child_theme_enqueue_scripts() {
	
	$parent_style = 'roam_mikado_default_style';
	
	wp_enqueue_style('roam_mikado_child_style', get_stylesheet_directory_uri() . '/style.css', array($parent_style));

}

add_action( 'wp_enqueue_scripts', 'roam_mikado_child_theme_enqueue_scripts' );

/**
 * Remove the slug from published post permalinks. Only affect our custom post type, though.
 */
function skyoz_remove_cpt_slug( $post_link, $post, $leavename ) {
 
    if ( 'tour-item' != $post->post_type || 'publish' != $post->post_status ) {
        return $post_link;
    }
 
    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
 
    return $post_link;
}
add_filter( 'post_type_link', 'skyoz_remove_cpt_slug', 10, 3 );
// *
//  * Have WordPress match postname to any of our public post types (post, page, race).
//  * All of our public post types can have /post-name/ as the slug, so they need to be unique across all posts.
//  * By default, WordPress only accounts for posts and pages where the slug is /post-name/.
//  *
//  * @param $query The current query.
 
function skyoz_add_cpt_post_names_to_main_query( $query ) {
	// Bail if this is not the main query.
	if ( ! $query->is_main_query() ) {
		return;
	}
	// Bail if this query doesn't match our very specific rewrite rule.
	if ( ! isset( $query->query['page'] ) || 2 !== count( $query->query ) ) {
		return;
	}
	// Bail if we're not querying based on the post name.
	if ( empty( $query->query['name'] ) ) {
		return;
	}
	// Add CPT to the list of post types WP will include when it queries based on the post name.
	$query->set( 'post_type', array( 'post', 'page', 'tour-item' ) );
}
add_action( 'pre_get_posts', 'skyoz_add_cpt_post_names_to_main_query' );

function _skyoz_remove_script_version( $src ){
	$parts = explode( '?', $src );
	return $parts[0];
	}
add_filter( 'script_loader_src', '_skyoz_remove_script_version', 15, 1 ); 
add_filter( 'style_loader_src', '_skyoz_remove_script_version', 15, 1 );

/*function skyoz_footer_scripts(){ ?>
 <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBp2qQXjS0hh0r7ZrW7eEFZfCISnSPNOJ0" type="text/javascript"></script>
<?php }
add_action('wp_footer', 'skyoz_footer_scripts');*/

function skyoz_change_post_object_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['tour-item']->labels;
    $labels->name = 'Skydive Locations';
    $labels->singular_name = 'Skydive Location';
    $labels->add_new = 'Add Skydive Location';
    $labels->add_new_item = 'Add Skydive Location';
    $labels->edit_item = 'Edit Skydive Location';
    $labels->new_item = 'Skydive Location';
    $labels->all_items = 'All Skydive Locations';
    $labels->view_item = 'View Skydive Location';
    $labels->search_items = 'Search Skydive Location';
    $labels->not_found = 'No Skydive Location found';
    $labels->not_found_in_trash = 'No Skydive Location found in Trash';    
}
add_action( 'init', 'skyoz_change_post_object_label', 999 );
function skyoz_change_post_menu_label() {
  global $menu;
  //print_r($menu); //Print menus and find out the index of your custom post type menu from it.
  $menu[11][0] = 'Skydive Locations'; // Replace the 27 with your custom post type menu index from displayed above $menu array 
}
add_action( 'admin_menu', 'skyoz_change_post_menu_label' );
