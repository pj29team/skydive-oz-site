<div class="skydive-options mkdf-tours-search-content">
   <div class="mkdf-tours-row mkdf-tr-normal-space mkdf-tours-columns-3">
      <div class="mkdf-tours-row-inner-holder" style="margin: 0;">
	 <h4 class="mkdf-gallery-title">
            Batemans Bay Vouchers
        </h4>
	<?php
    	$post_slug = 'skydive-batemans-bay-options'; 
	$sky_options = get_posts( 
	    array(
	        'post_type' => 'tour-item',
	        'posts_per_page' => -1,
                'post_status' => 'publish',
                'tax_query' => array(
			array(
				'taxonomy' => 'tour-category',
				'field'    => 'slug',
				'terms'    => $post_slug,
			),
		),
		'meta_key' => 'mkdf_tours_price',
		'orderby' => 'meta_value_num',
		'order' => 'ASC',
	    )
	);
	if ($sky_options): foreach ($sky_options as $post): setup_postdata($post);
	$old_price = get_post_meta(get_the_ID(), 'mkdf_tours_price', true);
	$new_price = get_post_meta(get_the_ID(), 'mkdf_tours_discount_price', true);
	$content = get_the_content();
	?>
		<?php if(has_post_thumbnail()) : ?>
		<div <?php post_class(array('mkdf-tours-revealing-item mkdf-tours-row-item',mkdf_tours_get_tour_rating_class())); ?>>
			<div class="mkdf-tours-revealing-item-image-holder">
				
				<div class="mkdf-tours-revealing-item-image">
					<?php the_post_thumbnail('full'); ?>
					<?php if ($new_price): ?>
					<span class="sale-now">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sale-corner-right.png" />
					</span>
					<?php endif; ?>
					<div class="mkdf-tours-revealing-item-content-holder">
						<div class="mkdf-tours-revealing-item-content-inner">
							<div class="mkdf-tours-revealing-title-holder">
								<h4 class="mkdf-tour-title">
									<span class="option-title"><?php the_title(); ?></span>
									<span class="option-sub">Tandem Skydive</span>
									
								</h4>
								<span class="mkdf-tours-revealing-item-price-holder">
								<?php if ($new_price):?>
									<span class="mkdf-tours-price-holder mkdf-tours-price-with-discount">
										<span class="mkdf-tours-item-price mkdf-tours-price-old">$<?php echo $old_price;?></span>
										<span class="mkdf-tours-item-discount-price mkdf-tours-item-price">$<?php echo $new_price;?></span>
									</span>
								<?php else: ?>
								<span class="mkdf-tours-price-holder">
									<span class="mkdf-tours-item-price ">$<?php echo $old_price;?></span>
								</span>
								<?php endif;?>
								</span>
							</div>
							<div class="book-now-btn">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/button.png" />
							</div>
							<?php if(has_excerpt()) : ?>
								<div class="mkdf-tours-revealing-item-excerpt">
									<div class="mkdf-tours-revealing-item-excerpt-inner">
										<?php the_excerpt(); ?>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<a id="option-link-<?php the_ID();?>" class="mkdf-tours-revealing-item-link" href="#skydive-options" data-id="<?php the_ID();?>"></a>
				<div id="option-content-<?php the_ID();?>" class="option-item">
				<div class="option-wrapper">
				<a href="#" class="pop-close">X</a>
				<div class="pop-title"><?php the_title(); ?> Tandem Skydive</div>
				<?php echo $content; ?>
				</div>
				</div>
				
			</div>
			</div>
		<?php endif; ?>
		<?php endforeach; wp_reset_postdata();?>
		<?php else: ?>
		   <p>There is no available Skydive Option for this location.</p>
		<?php endif; ?>
	</div>
      </div>
   </div>