<?php
/* Template Name: Blog Isotope Masonry */
$mkdf_sidebar_layout = roam_mikado_sidebar_layout();

get_header();
roam_mikado_get_title();
get_template_part( 'slider' );
$query_array = array(
	'post_status'    => 'publish',
	'post_type'      => 'post',
	'posts_per_page' => -1
);

$blog_query = new WP_Query( $query_array );
if ( is_archive() ) {
	global $wp_query;
	$blog_query = $wp_query;
}

$categories = get_categories();
?>
<style>
article { padding: 0 10px !important; }
.lm-wrap { display: table; margin: 0 auto 50px; }
.hidden{visibility:hidden;
  width:0px!important;
  height:0px!important;
  margin:0px!important;
  padding:0px!important;
  }
* {
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
}


/* ---- button ---- */

.button {
  display: inline-block;
  padding: 0.5em 1.0em;
  background: #EEE;
  border: none;
  border-radius: 7px;
  background-image: linear-gradient( to bottom, hsla(0, 0%, 0%, 0), hsla(0, 0%, 0%, 0.2) );
  color: #222;
  font-family: sans-serif;
  font-size: 16px;
  text-shadow: 0 1px white;
  cursor: pointer;
}

.button:hover {
  background-color: #8CF;
  text-shadow: 0 1px hsla(0, 0%, 100%, 0.5);
  color: #222;
}

.button:active,
.button.is-checked {
  background-color: #28F;
}

.button.is-checked {
  color: white;
  text-shadow: 0 -1px hsla(0, 0%, 0%, 0.8);
}

.button:active {
  box-shadow: inset 0 1px 10px hsla(0, 0%, 0%, 0.8);
}

/* ---- button-group ---- */

.button-group:after {
  content: '';
  display: block;
  clear: both;
}

.button-group .button {
  float: left;
  border-radius: 0;
  margin-left: 0;
  margin-right: 1px;
}

.button-group .button:first-child { border-radius: 0.5em 0 0 0.5em; }
.button-group .button:last-child { border-radius: 0 0.5em 0.5em 0; }

/* ---- isotope ---- */

.isotope {
  border: 1px solid #333;
}

/* clear fix */
.isotope:after {
  content: '';
  display: block;
  clear: both;
}
#filters { display: table; margin: 50px auto 0; padding: 0 25px; }
</style>
<script src="<?php echo get_stylesheet_directory_uri();?>/js/isotope.pkgd.min.js"></script>

<div class="mkdf-container mkdf-default-page-template">
	<?php do_action( 'roam_mikado_after_container_open' ); ?>
	
	<div id="filters" class="button-group filter-button-group">
		<button class="button" data-filter="*">show all</button>
		<?php foreach ($categories as $category): 
			if ( $category->slug == "packages" || $category->slug == "uncategorized" ) continue;
			?>
			<button class="button" data-filter=".category-<?php echo $category->slug;?>"><?php echo $category->name;?></button>
		<?php endforeach; wp_reset_postdata();?>
	</div>
	
	<div class="mkdf-container-inner clearfix">
		<div class="skyoz-blog mkdf-blog-holder mkdf-blog-masonry mkdf-blog-pagination-standard mkdf-masonry-images-fixed mkdf-blog-type-masonry mkdf-blog-three-columns mkdf-normal-space mkdf-blog-masonry-in-grid">
			<?php
			if ( $blog_query->have_posts() ) : while ( $blog_query->have_posts() ) : $blog_query->the_post();
				$format = get_post_format();
				get_template_part( 'post-formats/post', $format );
			endwhile;
			else:
				roam_mikado_get_module_template_part( 'templates/parts/no-posts', 'blog' );
			endif;
			
			wp_reset_postdata();
			?>
		</div>
	</div>
	<?php do_action( 'roam_mikado_before_container_close' ); ?>
</div>
<script>
$(document).ready(function () {
	var $container = $('.skyoz-blog').isotope({
	  // options
	  itemSelector: '.mkdf-item-space',
	  layoutMode: 'masonry'
	});
	
	// bind filter button click
	$('#filters').on('click', 'button', function() {
	var filterValue = $(this).attr('data-filter');
		$container.isotope({
			filter: filterValue
		});
	});
	
	// change is-checked class on buttons
	$('.button-group').each(function(i, buttonGroup) {
		var $buttonGroup = $(buttonGroup);
		$buttonGroup.on('click', 'button', function() {
		$buttonGroup.find('.is-checked').removeClass('is-checked');
		$(this).addClass('is-checked');
		});
	});
	
	//****************************
	// Isotope Load more button
	//****************************
	var initShow = 10; //number of items loaded on init & onclick load more button
	var counter = initShow; //counter for load more button
	var iso = $container.data('isotope'); // get Isotope instance
	
	loadMore(initShow); //execute function onload
	
	function loadMore(toShow) {
	$container.find(".hidden").removeClass("hidden");
	
	var hiddenElems = iso.filteredItems.slice(toShow, iso.filteredItems.length).map(function(item) {
	return item.element;
	});
	$(hiddenElems).addClass('hidden');
	$container.isotope('layout');
	
	//when no more to load, hide show more button
	if (hiddenElems.length == 0) {
	jQuery("#load-more").hide();
	} else {
	jQuery("#load-more").show();
	};
	
	}
	
	//append load more button
	$container.after('<div class="lm-wrap"><button id="load-more" class="button"> Load More</button></div>');
	
	//when load more button clicked
	$("#load-more").click(function() {
	if ($('#filters').data('clicked')) {
	//when filter button clicked, set initial value for counter
	counter = initShow;
	$('#filters').data('clicked', false);
	} else {
	counter = counter;
	};
	
	counter = counter + initShow;
	
	loadMore(counter);
	});
});
</script>

<?php get_footer(); ?>