<?php
/* Template Name: Voucher Location */
$mkdf_sidebar_layout = roam_mikado_sidebar_layout();

get_header();
roam_mikado_get_title();
get_template_part( 'slider' );
?>
<style>
.gift-cb { margin-top: 75px; }
.skydive-options .mkdf-tours-row.mkdf-tours-columns-3 .mkdf-tours-row-item { width: 32.33333%; padding: 5px; }
html.tour-pop-open{ overflow: hidden !important; }
header.tour-pop-open, header.tour-pop-open .mkdf-sticky-header, .skydive-vidphoto-table .wpb_gallery_slides.flexslider .flex-control-nav, .skydive-vidphoto-table .mkdf-price-table .mkdf-pt-button, .per-video { display:none !important; }
.skydive-batemans-bay .per-video { display: block !important; }
.option-item{
	display:none;
	background: rgba(0,0,0,0.75);
	position: fixed;
	width: 100%;
	z-index: 9999;
	height: 100%;
	top: 0;
	left: 0;
	opacity: 0;
	transition: visibility 0s, opacity 0.5s linear;
	-webkit-transition: visibility 0s, opacity 0.5s linear;
	-moz-transition: visibility 0s, opacity 0.5s linear;
}
.option-wrapper {
	max-width: 500px;
	margin: 6% auto 0;
	border-radius: 15px;
	position: fixed;
	left: 0;
	right: 0;
	background: #fff;
	max-height: 728px;
	overflow-y: auto;
	overflow-x: hidden;
}
.option-wrapper::-webkit-scrollbar {
    width: 10px;
}
.option-wrapper::-webkit-scrollbar-track {
    background: #f1f1f1;
    border-radius: 5px; 
}
.option-wrapper::-webkit-scrollbar-thumb {
    background: #888; 
}
.option-wrapper::-webkit-scrollbar-thumb:hover {
    background: #555; 
}
.pop-title {
    text-align: center;
    color: #4fb1e3;
    font-weight: 600;
    font-size: 16px;
    padding: 10px 0;
}
.pop-close {
position: absolute;
border-radius: 100%;
background: #4fb1e3;
padding: 5px;
font-size: 15px;
width: 16px;
height: 16px;
text-align: center;
line-height: 1;
font-weight: bold;
color: #fff;
right: -2px;
top: -2px;
}
.skydive-options { margin-bottom: 61px;}
.skydive-options .mkdf-tours-revealing-item .mkdf-tours-revealing-item-content-holder { padding: 0; }
.skydive-options .mkdf-tours-revealing-item .mkdf-tours-revealing-title-holder { padding: 15px 25px 10px; }
.skydive-options .mkdf-tours-revealing-item-excerpt { padding: 0px 15px; }
.skydive-options .book-now-btn { margin: 0 -2px; }
.skydive-options .mkdf-tour-title .option-title { font-size: 20px; }
.skydive-options .mkdf-tour-title .option-sub { font-size: 14px;letter-spacing: -1px; }
.skydive-options .mkdf-tours-price-with-discount .mkdf-tours-price-old.mkdf-tours-item-price { font-size: 14px; }
.skydive-options .mkdf-tours-price-holder { font-size: 18px; }
.skydive-options .mkdf-tours-revealing-item-excerpt-inner { font-size: 12px; }
.skydive-options .mkdf-tours-item-price { color: #7ed16f !important; }
.skydive-options .sale-now { position: absolute;top: 0;right: 0px;}
.skydive-options .mkdf-gallery-title, .gift-cb-title { text-align: center; }

.mkdf-tour-gallery, .mkdf-tour-gallery > .vc_row { margin: 0 !important; }

.skydive-vidphoto-table .wpb_column .vc_column-inner { padding: 0 5px; }
.skydive-vidphoto-table .wpb_gallery_slides.flexslider { margin: 0; border: 0; }
.skydive-vidphoto-table .mkdf-price-table .mkdf-pt-inner ul li { padding: 10px 15px; }
.skydive-vidphoto-table .mkdf-price-table .mkdf-pt-content ul li { padding: 10px 0; }
.skydive-vidphoto-table .mkdf-price-table .mkdf-pt-title { min-height: 118px; }
.skydive-vidphoto-table .mkdf-price-table .mkdf-pt-mark { min-height: 42px; } 
.skydive-vidphoto-table .mkdf-price-table .mkdf-pt-inner > ul { padding-bottom: 20px; }

@media ( max-width: 1200px ) {
	.skydive-vidphoto-table .wpb_column { width: 50% !important; }
}

@media ( max-width: 768px ){
	.skydive-options .mkdf-tours-row.mkdf-tours-columns-3 .mkdf-tours-row-item { width: auto; }
	.option-wrapper { position: relative; border: 1px solid #e1e1e1; max-width: 100% !important; overflow: auto !important;}
	.option-item { background: transparent; position: relative; }
	html.tour-pop-open{ overflow: auto !important; }
	.mkdf-tour-item-single-holder .mkdf-grid-col-9 { padding: 0 !important; }
	.book-now-btn img { width: 100%; }
	.skydive-options .mkdf-tour-title .option-title { font-size: 24px; }
	.skydive-options .mkdf-tour-title .option-sub { font-size: 16px;letter-spacing: -1px; }
	.skydive-options .mkdf-tours-price-with-discount .mkdf-tours-price-old.mkdf-tours-item-price { font-size: 16px; }
	.skydive-options .mkdf-tours-price-holder { font-size: 20px; }
	.skydive-options .mkdf-tours-revealing-item-excerpt-inner { font-size: 14px; }
}

@media ( max-width: 767px ){
	.skydive-vidphoto-table .wpb_column { width: 100% !important; }
}
</style>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/pluginJS.js"></script>
<div class="mkdf-container mkdf-default-page-template">
	<?php do_action( 'roam_mikado_after_container_open' ); ?>
	
	<div class="mkdf-container-inner clearfix">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="mkdf-grid-row">
				<div <?php echo roam_mikado_get_content_sidebar_class(); ?>>
				<div class="gift-cb vc_row wpb_row vc_row-fluid">
					<h3 class="gift-cb-title">Select a height option, then click Gift Voucher.</h3>
					<div class="wpb_column vc_column_container vc_col-lg-6">
						<div class="vc_column-inner ">
							<div class="wpb_wrapper">
								<?php get_template_part('skydive','canberra'); ?>
							</div>
						</div>
					</div>
					<div class="wpb_column vc_column_container vc_col-lg-6">
						<div class="vc_column-inner ">
							<div class="wpb_wrapper">
								<?php get_template_part('skydive','batemans'); ?>
							</div>
						</div>
					</div>
				</div>
					<?php
						the_content();
						do_action( 'roam_mikado_page_after_content' );
					?>
				</div>
				<?php if ( $mkdf_sidebar_layout !== 'no-sidebar' ) { ?>
					<div <?php echo roam_mikado_get_sidebar_holder_class(); ?>>
						<?php get_sidebar(); ?>
					</div>
				<?php } ?>
			</div>
		<?php endwhile; endif; ?>
	</div>
	
	<?php do_action( 'roam_mikado_before_container_close' ); ?>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
function option_pop(id){
  var option_id = '#option-content-' + id; 
  jQuery(option_id).css({
  	'display' : 'block',
  	'opacity' : '1'
  });
    jQuery(document).on('click','.pop-close',function(e){
      jQuery(option_id).hide();
      jQuery('html').removeClass('tour-pop-open');
      jQuery('header').removeClass('tour-pop-open');
      e.preventDefault();
  });
}
jQuery(document).on('click','.mkdf-tours-revealing-item-link',function(e){
var data_id = jQuery(this).data('id');
option_pop(data_id);
jQuery('html').addClass('tour-pop-open');
jQuery('header').addClass('tour-pop-open');
e.preventDefault();
jQuery(this).parent().find('iframe').attr( 'src', function ( i, val ) { return val; });
});
});
</script>

<?php get_footer(); ?>